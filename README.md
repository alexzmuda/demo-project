# Demo Task

```
git clone
composer install
php artisan serve
```

then i.e. 

```shell
curl --location 'http://localhost:8000/demo_endpoint' \
--header 'Content-Type: application/json' \
--data '{ "username": "Alex" }'
```

returns
```shell
{
    "username": "Alex",
    "ip_address": "127.0.0.1",
    "date": "2023-11-06"
}
```

and entry in storage/logs/laravel.log
```shell
[2023-11-06 13:17:24] local.INFO: IP Address: 127.0.0.1 accessed route: demo_endpoint  
```

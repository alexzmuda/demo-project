<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class DemoController extends Controller
{
    public function demoEndpoint(Request $request): JsonResponse
    {
        $data = [
            'username' => $request->input('username'),
            'ip_address' => $request->ip(),
            'date' => Carbon::now()->format('Y-m-d'),
        ];
        return response()->json($data, Response::HTTP_OK);
    }
}

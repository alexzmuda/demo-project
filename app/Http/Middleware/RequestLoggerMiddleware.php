<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Log;

class RequestLoggerMiddleware
{
    public function handle($request, Closure $next)
    {
        Log::info('IP Address: ' . $request->ip() . ' accessed route: ' . $request->path());

        return $next($request);
    }
}
